<?php

class Location
{
    public $num_resa;
    public $id_client;
    public $date_resa;
    public $date_depart;
    public $date_retour;

    public function __construct($num_resa_constr, $id_client_constr, $date_resa_constr, $date_depart_constr,$date_retour_constr)
    {
        $this->num_resa = $num_resa_constr;
        $this->id_client = $id_client_constr;
        $this->date_resa = $date_resa_constr;
        $this->date_depart = $date_depart_constr;
        $this->date_retour = $date_retour_constr;
    }
}
