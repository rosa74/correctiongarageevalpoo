<?php

class Voiture {
    public $id;
    public $modele;
    public $couleur;
    public $num_immat;
    public $louable;

    public function __construct($couleur_constr,$modele_constr,$num_immat_constr,$louable_constr)
    {
        $this->couleur = $couleur_constr;
        $this->modele = $modele_constr;
        $this->num_immat = $num_immat_constr;
        $this->louable = $louable_constr;
    }
}