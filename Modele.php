<?php

class Modele {
    private $id;
    private $libelle_type;
    private $constructeur;
    private $nom;
    private $chevaux;
    private $combustible;
    private $consommation;
    private $volume_coffre;
    private $nb_place;
    private $nb_porte;
    private $boite_vitesse;

    public function __construct($libelle_type_constr,$constructeur_constr,$nom_constr,$chevaux_constr,$combustible_constr,$consommation_constr,$volume_coffre_constr,
    $nb_place_constr,$nb_porte_constr,$boite_vitesse_constr)
    {
        $this->type = $libelle_type_constr;
        $this->constructeur = $constructeur_constr;
        $this->nom = $nom_constr;
        $this->chevaux = $chevaux_constr;
        $this->combustible = $combustible_constr;
        $this->consommation = $consommation_constr;
        $this->volume_coffre = $volume_coffre_constr;
        $this->nb_place = $nb_place_constr;
        $this->nb_porte = $nb_porte_constr;
        $this->boite_vitesse = $boite_vitesse_constr;
    }
    //Getters & Setters
    public function getId()
    {
        return $this->id;
    }
    public function setId($id_saisie)
    {
        $this->id = $id_saisie;
    }
    public function getLibelle_type()
    {
        return $this->libelle_type;
    }
    public function setLibelle_type($libelle_type_saisie)
    {
        $this->pseudo = $libelle_type_saisie;
    }
    public function setConstructeur($constructeur_saisie)
    {
        $this->constructeur = $constructeur_saisie;
    }
    public function getConstructeur()
    {
        return $this->constructeur;
    }
    public function setNom($nom_saisie)
    {
        $this->nom = $nom_saisie;
    }
    public function getNom()
    {
        return $this->nom;
    }
    public function getChevaux()
    {
        return $this->chevaux;
    }
    public function setChevaux($chevaux_saisie)
    {
        $this->chevaux = $chevaux_saisie;
    }
    public function getCombustible()
    {
        return $this->combustible;
    }
    public function setCombustible($combustible_saisie)
    {
        $this->combustible= $combustible_saisie;
    }
    public function getConsommation()
    {
        return $this->consommation;
    }
    public function setConsommation($consommation_saisie)
    {
        $this->consommation = $consommation_saisie;
    }
    public function getVolume_coffre()
    {
        return $this->volume_coffre;
    }
    public function setVolume_coffre($volume_coffre_saisie)
    {
        $this->volume_coffre = $volume_coffre_saisie;
    }
    public function getNb_place()
    {
        return $this->nb_place;
    }
    public function setNb_place($nb_place_saisie)
    {
        $this->nb_place= $nb_place_saisie;
    }
    public function getNb_porte()
    {
        return $this->nb_porte;
    }
    public function setNb_porte($nb_porte_saisie)
    {
        $this->nb_porte = $nb_porte_saisie;
    }
    public function getBoite_vitesse()
    {
        return $this->boite_vitesse;
    }
    public function setBoite_vitesse($boite_vitesse_saisie)
    {
        $this->boite_vitesse = $boite_vitesse_saisie;
    }
    
}

