<?php

class Client {
    public $id;
    public $nom;
    public $prenom;
    public $adresse;
    public $code_postal;
    public $ville;
    public $telephone;
    public $email;
    public $date_naiss;
    public $num_permis;
    public $date_permis;


    public function __construct($nom_constr,$prenom_constr,$adresse_constr,$code_postal_constr,$ville_constr,$telephone_constr,$email_constr,$date_naiss_constr,$num_permis_constr,$date_permis_constr)
    {
        $this->nom = $nom_constr;
        $this->prenom = $prenom_constr;
        $this->adresse = $adresse_constr;
        $this->code_postal= $code_postal_constr;
        $this->ville = $ville_constr;
        $this->telephone=$telephone_constr;
        $this->email=$email_constr;
        $this->date_naiss=$date_naiss_constr;
        $this->num_permis=$num_permis_constr;
        $this->date_permis=$date_permis_constr;
        
    }

         
}